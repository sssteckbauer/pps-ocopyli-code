000001*==========================================================%      DU040
000002*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      DU040
000003*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - DU040        =%      DU040
000004*=                                                        =%      DU040
000005*==========================================================%      DU040
000006*==========================================================%      DS651
000007*=    COPYMEMBER: CPWSDB55                                =%      DS651
000008*=    CHANGE # DS651        PROJ. REQUEST: REL654         =%      DS651
000009*=    NAME: MARI MCGEE      MODIFICATION DATE: 03/11/92   =%      DS651
000010*=                                                        =%      DS651
000011*=    DESCRIPTION:                                        =%      DS651
000012*=     ADD  XDBS-5545-FYTD-FWT-GROSS                      =%      DS651
000013*=     CHANGES OCCURS 44 TO OCCURS 45                     =%      DS651
000014*==========================================================%      DS651
000015**************************************************************/   37640583
000016*  COPYMEMBER: CPWSDB55                                      */   37640583
000017*  REL: 0583 __REF: 580__ SERVICE REQUEST(S): _____3764____  */   37640583
000018*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___07/03/91__  */   37640583
000019*  DESCRIPTION:                                              */   37640583
000020*                                                            */   37640583
000021*    -CHANGED XDBS-5500-GRS-ELEMNTS  OCCURS CLAUSE TO 44     */   37640583
000022*     XDBS-5500-FILLER-OP     PICTURE X(56).                 */   37640583
000023*                                                            */   37640583
000024**************************************************************/   37640583
000025**************************************************************/   37640580
000026*  COPYMEMBER: CPWSDB55                                      */   37640580
000027*  RELEASE: ___0580______ SERVICE REQUEST(S): _____3764____  */   37640580
000028*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___05/17/91__  */   37640580
000029*  DESCRIPTION:                                              */   37640580
000030*   - ADDED XDBS-FTYD-RET-GROSS                              */   37640580
000031**************************************************************/   37640580
000032**************************************************************/   01900580
000033*  COPYMEMBER: CPWSDB55                                      */   01900580
000034*  RELEASE: ___0580______ SERVICE REQUEST(S): ____10190____  */   01900580
000035*  NAME:___LOU DULANEY___ MODIFICATION DATE:  ___06/12/91__  */   01900580
000036*  DESCRIPTION:                                              */   01900580
000037*   - ADDED XDBS-YTD-SFHBR-GROSS                             */   01900580
000040**************************************************************/   01900580
000100**************************************************************/   37550521
000200*  COPYMEMBER: CPWSDB55                                      */   37550521
000300*  RELEASE # ____0521____  SERVICE REQUEST NO(S)____3755____ */   37550521
000400*  NAME      _______KXK__  MODIFICATION DATE    __11/19/90__ */   37550521
000500*  DESCRIPTION                                               */   37550521
000600*   - SPLIT FICA OUT INTO OASDI, AND MEDICARE                */   37550521
000800**************************************************************/   37550521
000801**************************************************************/   40070508
000802*  COPYMEMBER: CPWSDB55                                      */   40070508
000803*  RELEASE # ____0508____  SERVICE REQUEST NO(S)____4007____ */   40070508
000804*  NAME      _L_DULANEY__  MODIFICATION DATE    __10/05/90__ */   40070508
000805*  DESCRIPTION                                               */   40070508
000806*   - ADDED DEFINED CONTRIBUTION PLAN (DCP) ELEMENTS FOR     */   40070508
000807*     UCRP REVAMP.                                           */   40070508
000808**************************************************************/   40070508
000809**************************************************************/   40180480
000810*  COPYMEMBER: CPWSDB55                                      */   40180480
000811*  RELEASE: ____0480____  SERVICE REQUEST(S): ____4018____   */   40180480
000812*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __06/05/90__   */   40180480
000813*  DESCRIPTION:                                              */   40180480
000814*                                                            */   40180480
000815*    - DELETED PREVIOUS 'CD' LINES                           */   40180480
000816*    - DELETED CODE PREVIOUSLY COMMENTED-OUT AND MARKED 'CD' */   40180480
000817*    - ADDED NEW DATA ELEMENTS FOR FCP DISCRIMINATION        */   40180480
000818*      TESTING:                                              */   40180480
000819*      - XDBS-5540-FCP-TST-TOT-GRS                           */   40180480
000820*      - XDBS-5541-FCP-TST-TAX-GRS                           */   40180480
000821**************************************************************/   40180480
000822**************************************************************/   36020424
000823*  COPYMEMBER: CPWSDB55                                      */   36020424
000824*  RELEASE: ____0424____  SERVICE REQUEST(S): ____3602____   */   36020424
000825*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __04/24/89__   */   36020424
000826*  DESCRIPTION:                                              */   36020424
000827*                                                            */   36020424
000828*    - ADD NEW DATA ELEMENTS FOR TAX REPORTING               */   36020424
000829*      - XDBS-5537-YTD-TAX-TREATY-GRS                        */   36020424
000900*      - XDBS-5538-YTD-ALT-TT-GRS                            */   36020424
001000*      - XDBS-5539-ETD-TAX-TREATY-GRS                        */   36020424
001100*      - SPACE CAME FROM GROSSES WHICH WERE ORIGINALLY       */   36020424
001200*        FILLER.                                             */   36020424
001300**************************************************************/   36020424
001301**************************************************************/   30930413
001302*  COPYMEMBER: CPWSDB55                                      */   30930413
001303*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
001304*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
001305*  DESCRIPTION                                               */   30930413
001306*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
001307**************************************************************/   30930413
001308**************************************************************/   45260381
001309*  COPYMEMBER: CPWSDB55                                      */   45260381
001310*  RELEASE: ____0381____  SERVICE REQUEST(S): ____4526____   */   45260381
001311*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __10/13/88__   */   45260381
001312*  DESCRIPTION:                                              */   45260381
001313*                                                            */   45260381
001314*    - ADD NEW GROSS BALANCE FOR EXECUTIVE LIFE PROGRAM      */   45260381
001315*      - XDBS-5518-EXEC-LIFE-INC, IN PLACE OF FILLER         */   45260381
001316**************************************************************/   45260381
001317**************************************************************/   14140251
001318*  COPYID:   CPWSDB55                                        */   14140251
001319*  RELEASE # _0251_______ SERVICE REQUEST NO(S)____1414______*/   14140251
001320*  NAME ___JLT_________   MODIFICATION DATE ____10/22/86_____*/   14140251
001400*  DESCRIPTION                                               */   14140251
001500*     - ADDED  TWO NEW ELEMENTS:                             */   14140251
001600*              - YTD MEDICARE GROSS (D.E.#5510) AND          */   14140251
001700*              - QTD MEDICARE GROSS (D.E.#5517).             */   14140251
001800*     - DELETED TWO ELEMENTS:                                */   14140251
001900*              - YTD EMPLOYER FICA GROSS(D.E. #5504) AND     */   14140251
002000*              - QTD EMPLOYER FICA GROSS(D.E. #5514).        */   14140251
002100**************************************************************/   14140251
002200**************************************************************/   14120250
002300*  COPY MODULE:  CPWSDB55                                    */   14120250
002400*  RELEASE # ___0250___   SERVICE REQUEST NO(S)____1412______*/   14120250
002500*  NAME ___DBS_________   MODIFICATION DATE ____11/10/86_____*/   14120250
002600*  DESCRIPTION                                               */   14120250
002700*  REMOVE OBSOLETE ELEMENTS 5517 AND 5518.                   */   14120250
002800**************************************************************/   13070163
002900*  COPYMEMBER: CPWSDB55                                      */   13070163
003000*  RELEASE # ___0163___   SERVICE REQUEST NO(S) 1307         */   13070163
003100*  NAME ___JLT_________   MODIFICATION DATE ____10/25/85_____*/   13070163
003200*  DESCRIPTION                                               */   13070163
003300*    ADDED DATA ELEMENT 5536, LAST MONTH OF GTN (CALC RTN    */   13070163
003400*    "15') ACTIVITY.                                         */   13070163
003500***********************************************************  */   13070163
003600**************************************************************/   13190159
003700*  COPY MODULE:  CPWSDB55                                    */   13190159
003800*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
003900*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
004000*  DESCRIPTION                                               */   13190159
004100*  FOR PHASE 2 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190159
004200*  USER-CODED TRANSACTION INPUT RECORD LENGTHS WILL BE       */   13190159
004300*  EXPANDED BY 22 BYTES, AND RECORDS AND WORK AREAS          */   13190159
004400*  RELATING TO THE EDB FILE WILL BE EXPANDED AS WELL.        */   13190159
004500*  THE EDB FILE 5500 SEGMENTS WILL BE EXPANDED               */   13190159
004600*  BY 66 BYTES FOR OFFICE OF THE PRESIDENT USE.              */   13190159
004700**************************************************************/   13190159
004800     SKIP2                                                        13190159
004900**************************************************************/   22430132
005000*  COPYMEMBER: CPWSDB55                                      */   22430132
005100*  RELEASE # ___0132___   SERVICE REQUEST NO(S) 2243         */   22430132
005200*  NAME ___JLT_________   MODIFICATION DATE ____11/25/84_____*/   22430132
005300*  DESCRIPTION                                               */   22430132
005400*    ADDED DATA ELEMENT 5535, YEAR-TO-DATE TAXABLE LIFE      */   22430132
005500*    PREMIUM.                                                */   22430132
005600***********************************************************  */   22430132
005700*                                                          ***/   22430132
005800**************************************************************/   21650115
005900*  COPY MODULE:  CPWSDB55                                    */   21650115
006000* RELEASE #  _______0115___ SERVICE REQUESTS NO 2165         */   21650115
006100* NAME___ JLT   ___________ MODIFICATION DATE___6/25/84___   */   21650115
006200* DESCRIPTION                                                */   21650115
006300*    THE USAGE OF DATA ELEMENTS 5530 THROUGH 5534 WAS        */   21650115
006400*    MODIFIED TO CONTAIN THE RETIREMENT RATE FOR UCRS AND    */   21650115
006500*    PERS PLANS INSTEAD OF THE DEMISED PLAN 7 AMOUNT.         /   21650115
006600**************************************************************/   21650115
006700*                                                            */   21650115
006800*    COPYID=CPWSDB55                                              CPWSDB55
006900*01  XDBS-GROSS-DATA55.                                           30930413
007000*                                                                *CPWSDB55
007100******************************************************************CPWSDB55
007200*                G R O S S     S E G M E N T   5 5 0 0           *CPWSDB55
007300******************************************************************CPWSDB55
007400*                                                                *CPWSDB55
007500     03  XDBS-DELETE-55              PICTURE X.                   CPWSDB55
007600         88  XDBS-SEG-DELETED-55             VALUE HIGH-VALUES.   CPWSDB55
007700     03  XDBS-KEY-55.                                             CPWSDB55
007800         05  XDBS-ID-NUMBER-55       PICTURE X(9).                CPWSDB55
007900         05  XDBS-SEGMENT-55         PICTURE X(4).                CPWSDB55
008000     03  XDBS-GROSS-ELEMENTS.                                     CPWSDB55
008100         05  XDBS-5501-YTD-TTL-GROSS PICTURE S9(7)V99    COMP-3.  CPWSDB55
008200         05  XDBS-5502-FWT-YTD-GROSS PICTURE S9(7)V99    COMP-3.  CPWSDB55
012400*********05  XDBS-5503-FICA-YTD-GRS  PICTURE S9(7)V99    COMP-3.  37550521
012410         05  XDBS-5503-OASDI-YTD-GRS PICTURE S9(7)V99    COMP-3.  37550521
012500         05  XDBS-5504-NDIP-YTD-GRS  PICTURE S9(7)V99    COMP-3.  40070508
012600*********05  FILLER                  PICTURE S9(7)V99    COMP-3.  40070508
012601         05  XDBS-5505-RETR-YTD-GRS  PICTURE S9(7)V99    COMP-3.  CPWSDB55
012602         05  XDBS-5506-SWT-YTD-GRS   PICTURE S9(7)V99    COMP-3.  CPWSDB55
012603         05  XDBS-5507-NTX-YTD-GRS   PICTURE S9(7)V99    COMP-3.  CPWSDB55
013000         05  XDBS-5508-CURR-DCP-RATE PICTURE S9(7)V99    COMP-3.  40070508
013100*********05  XDBS-5508-CETA-YTD-GRS  PICTURE S9(7)V99    COMP-3.  40070508
013101         05  XDBS-5509-UI-YTD-GRS    PICTURE S9(7)V99    COMP-3.  CPWSDB55
013102         05  XDBS-5510-MEDICR-YTD-GR PICTURE S9(7)V99    COMP-3.  14140251
013400         05  XDBS-5511-1MTH-DCP-RATE PICTURE S9(7)V99    COMP-3.  40070508
013500*********05  FILLER                  PICTURE S9(7)V99    COMP-3.  40070508
013600         05  XDBS-5512-FWT-QTD-GRS   PICTURE S9(7)V99    COMP-3.  CPWSDB55
013700*********05  XDBS-5513-FICA-QTD-GRS  PICTURE S9(7)V99    COMP-3.  37550521
013710         05  XDBS-5513-OASDI-QTD-GRS PICTURE S9(7)V99    COMP-3.  37550521
013800         05  XDBS-5514-2MTH-DCP-RATE PICTURE S9(7)V99    COMP-3.  40070508
013900*********05  FILLER                  PICTURE S9(7)V99    COMP-3.  40070508
014000         05  XDBS-5515-3MTH-DCP-RATE PICTURE S9(7)V99    COMP-3.  40070508
014100*********05  XDBS-5515-CETA-QTD-GRS  PICTURE S9(7)V99    COMP-3.  40070508
014101         05  XDBS-5516-UI-QTD-GRS    PICTURE S9(7)V99    COMP-3.  CPWSDB55
014102         05  XDBS-5517-MEDICR-QTD-GR PICTURE S9(7)V99    COMP-3.  14140251
014103         05  XDBS-5518-EXEC-LIFE-INC PICTURE S9(7)V99    COMP-3.  45260381
014104         05  XDBS-5519-OTHER-INCOME  PICTURE S9(7)V99    COMP-3.  CPWSDB55
014105         05  XDBS-5520-CURR-RETR-GRS PICTURE S9(7)V99    COMP-3.  CPWSDB55
014106         05  XDBS-5521-1MTH-GRS-RETR PICTURE S9(7)V99    COMP-3.  CPWSDB55
014107         05  XDBS-5522-2MTH-GRS-RETR PICTURE S9(7)V99    COMP-3.  CPWSDB55
014108         05  XDBS-5523-3MTH-GRS-RETR PICTURE S9(7)V99    COMP-3.  CPWSDB55
014109         05  XDBS-5524-4MTH-GRS-RETR PICTURE S9(7)V99    COMP-3.  CPWSDB55
014110         05  XDBS-5525-CURR-RED-GRS  PICTURE S9(7)V99    COMP-3.  CPWSDB55
014111         05  XDBS-5526-1MTH-RED-AMT  PICTURE S9(7)V99    COMP-3.  CPWSDB55
014112         05  XDBS-5527-2MTH-RED-AMT  PICTURE S9(7)V99    COMP-3.  CPWSDB55
014113         05  XDBS-5528-3MTH-RED-AMT  PICTURE S9(7)V99    COMP-3.  CPWSDB55
014114         05  XDBS-5529-4MTH-RED-AMT  PICTURE S9(7)V99    COMP-3.  CPWSDB55
014115         05  XDBS-5530-CURR-RTR-RATE PICTURE S9(7)V99    COMP-3.  21650115
014116         05  XDBS-5531-1MTH-RTR-RATE PICTURE S9(7)V99    COMP-3.  21650115
014117         05  XDBS-5532-2MTH-RTR-RATE PICTURE S9(7)V99    COMP-3.  21650115
014118         05  XDBS-5533-3MTH-RTR-RATE PICTURE S9(7)V99    COMP-3.  21650115
014119         05  XDBS-5534-4MTH-RTR-RATE PICTURE S9(7)V99    COMP-3.  21650115
014120         05  XDBS-5535-YTD-TLP-ADJ   PICTURE S9(7)V99    COMP-3.  22430132
014121         05  XDBS-5536-LST-MN-GTN-ACT    PIC S9(7)V99    COMP-3.  13070163
014122         05  XDBS-5537-YTD-TAX-TREATY-GRS PIC S9(7)V99   COMP-3.  36020424
014200         05  XDBS-5538-YTD-ALT-TT-GRS     PIC S9(7)V99   COMP-3.  36020424
014300         05  XDBS-5539-ETD-TAX-TREATY-GRS PIC S9(7)V99   COMP-3.  36020424
016600*****                                                          CD 40070508
016601         05  XDBS-5540-FCP-TST-TOT-GRS    PIC S9(7)V99   COMP-3.  40180480
016602         05  XDBS-5541-FCP-TST-TAX-GRS    PIC S9(7)V99   COMP-3.  40180480
016900         05  XDBS-5542-4MTH-DCP-RATE      PIC S9(7)V99   COMP-3.  40070508
017000*********05  FILLER                       PIC S9(7)V99   COMP-3.  40070508
017010         05  XDBS-5543-FYTD-RET-GROSS     PIC S9(7)V99   COMP-3.  37640580
017020         05  XDBS-5544-YTD-SFHBR-GROSS    PIC S9(7)V99   COMP-3.  01900580
017030         05  XDBS-5545-FYTD-FWT-GROSS     PIC S9(7)V99   COMP-3.  DS651
017100*****    05  XDBS-5570-FILLER-OP1    PICTURE X(30).               37640580
017110*****    05  XDBS-5570-FILLER-OP1    PICTURE X(20).               DS651
017111         05  XDBS-5570-FILLER-OP1    PICTURE X(15).               DS651
017112         05  XDBS-5580-FILLER-OP2    PICTURE X(30).               13190159
017113         05  XDBS-5590-FILLER-OP3    PICTURE X(06).               13190159
017114*                                                                *CPWSDB55
017115******************************************************************CPWSDB55
017116*            G R O S S   F O R   S U B S C R I P T I N G         *CPWSDB55
017117******************************************************************CPWSDB55
017118*                                                                *CPWSDB55
017119     03  XDBS-GRS-ELEMENTS   REDEFINES   XDBS-GROSS-ELEMENTS.     CPWSDB55
018000*****    05  XDBS-5500-GRS-ELEMNTS   OCCURS  42  TIMES.           37640580
020700*****    05  XDBS-5500-GRS-ELEMNTS   OCCURS  43  TIMES.           37640583
020710******   05  XDBS-5500-GRS-ELEMNTS   OCCURS  44  TIMES.           DS651 83
020711         05  XDBS-5500-GRS-ELEMNTS   OCCURS  45  TIMES.           DS651
020712             07  XDBS-5500-GRS-AMT   PICTURE S9(7)V99    COMP-3.  CPWSDB55
020713*****    05  XDBS-5500-FILLER-OP     PICTURE X(66).               37640580
021000*****    05  XDBS-5500-FILLER-OP     PICTURE X(61).               37640583
021100*****    05  XDBS-5500-FILLER-OP     PICTURE X(56).               DS651
021200         05  XDBS-5500-FILLER-OP     PICTURE X(51).               DS651
